package com.example.calcapp.calcapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.example.calcapp.calcapplication.calculations.EulerTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.example.calcapp.calcapplication.R.id.editText4;

public class CalcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);
    }

    public void readFileClick(View v) {

        final ArrayList<String> list = new ArrayList<String>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("skaiciai.txt"), "UTF-8"));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                list.add(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        ListView listView = (ListView) findViewById(R.id.listas);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, list);

        listView.setAdapter(adapter);
    }

        public void calcSumClick(View v)
    {
        TextView field1 = (TextView)findViewById(R.id.editText1);
        TextView field2 = (TextView)findViewById(R.id.editText2);
        TextView field3 = (TextView)findViewById(R.id.editText3);

        EulerTask ETask = new EulerTask();
        int Sum = ETask.calcSum(Integer.parseInt(field1.getText().toString()), Integer.parseInt(field2.getText().toString()));
        field3.setText(Integer.toString(Sum));

        EditText editText4 = (EditText)findViewById(R.id.editText4);
        editText4.setText(Integer.toString(Sum));

        ListView listView = (ListView) findViewById(R.id.listas);

        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 20; i++) {
            list.add(Integer.toString(i));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, list);

        listView.setAdapter(adapter);

    }
}
